#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
// Event info
#include <xAODEventInfo/EventInfo.h>
// xAOD core
#include <xAODCore/AuxContainerBase.h>
// Truth particles
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthParticle.h>
// Jets                                                                         
#include <xAODJet/JetContainer.h>
#include <xAODJet/Jet.h>
// Vertex
#include <xAODTruth/TruthVertexContainer.h>

#include <PMGTools/PMGTruthWeightTool.h>

// ROOT includes
#include <TH1F.h>
#include <TLorentzVector.h>
#include <TVector3.h>

// C++
#include <iostream>
#include <vector>
#include <algorithm> 
#include <typeinfo> 
// TTHbb analysis
//#include "TTHbbAnalysis/TTHbbToolManager/OfflineCPVariables.h"
/*#include "TTHbbObjects/TTHbbUtils.h"
#include "TTHbbConfiguration/GlobalConfiguration.h"

#include "MVAVariables/JetOrderingTool.h"
*/

using namespace std;

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator),
    m_weightTool("PMGTools::PMGTruthWeightTool/PMGTruthWeightTool",this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  /*asg::AnaToolHandle<PMGTools::IPMGTruthWeightTool> m_weightTool;
  m_weightTool.setTypeAndName("PMGTools::PMGTruthWeightTool/PMGTruthWeightTool");
  ATH_CHECK(m_weightTool.retrieve());
  */

  m_weightTool.declarePropertyFor(this, "PMGTruthWeightTool");
}



StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK(book(TH1F("higgs_pt","higgs_pt",100,0,500)));
  ANA_CHECK(book(TH1F("thad_pt","thad_pt",100,0,500)));
  ANA_CHECK(book(TH1F("tlep_pt","tlep_pt",100,0,500)));

  ANA_CHECK(book(TH1F("dEta_tt","dEta_tt",50,0,5)));
  ANA_CHECK(book(TH1F("dEta_bb","dEta_bb",50,0,5)));
  ANA_CHECK(book(TH1F("dEta_jl","dEta_jl",50,0,5)));
  ANA_CHECK(book(TH1F("dPhi_tt","dPhi_tt",50,0,TMath::Pi())));
  ANA_CHECK(book(TH1F("b4_tt","b4_tt",50,-1,1)));

  ANA_CHECK(book(TH1F("lep_pt","lep_pt",50,0,250)));

  ANA_CHECK(book(TH1F("cutflow","cutflow",5,-.5,4.5)));

  ANA_CHECK(book(TH1F("n_jets","n_jets",10,-.5,9.5)));
  ANA_CHECK(book(TH1F("n_bjets","n_bjets",10,-.5,9.5)));

  // Book histos for angular variables
  // 6j regions
  ANA_CHECK(book(TH1F("b4_lep_quarkWhighPt","b4_lep_quarkWhighPt",100,-1,1)));
  ANA_CHECK(book(TH1F("sin_tB_wrt_ttHxsin_btA_wrt_H_in_tAH_seq","sin_tB_wrt_ttHxsin_btA_wrt_H_in_tAH_seq",100,0,1)));
  ANA_CHECK(book(TH1F("dR_bH_highPt_bhad","dR_bH_highPt_bhad",100,0,6)));
  ANA_CHECK(book(TH1F("dR_lep_bH_highPt","dR_lep_bH_highPt",100,0,6)));
  ANA_CHECK(book(TH1F("M_Wlep_bthad","M_Wlep_bthad",100,0,1000)));
  ANA_CHECK(book(TH1F("cos_b1H_wrt_tA_in_HtA_seqxcos_b2H_wrt_tA_in_HtA_seq","cos_b1H_wrt_tA_in_HtA_seqxcos_b2H_wrt_tA_in_HtA_seq",100,0,1)));
  ANA_CHECK(book(TH1F("sin_t1_wrt_ttHxsin_Wt2_wrt_H_in_t1H_seq","sin_t1_wrt_ttHxsin_Wt2_wrt_H_in_t1H_seq",100,0,1)));
  ANA_CHECK(book(TH1F("m_ljjjjj_MaxPt","m_ljjjjj_MaxPt",100,0,1000)));
  ANA_CHECK(book(TH1F("Mbb_MaxPt_Sort4","Mbb_MaxPt_Sort4",100,0,1000)));
  ANA_CHECK(book(TH1F("Mbb_MindR_Sort4","Mbb_MindR_Sort4",100,0,1000)));
  ANA_CHECK(book(TH1F("Mbb_MaxM_Sort4","Mbb_MaxM_Sort4",100,0,1000))); 
  
  // 5j regions
  ANA_CHECK(book(TH1F("dR_H_tlep","dR_H_tlep",100,0,6)));
  ANA_CHECK(book(TH1F("M_H_btlep","M_H_btlep",100,0,500)));
  ANA_CHECK(book(TH1F("sin_ltA_wrt_H_in_tAH_seq","sin_ltA_wrt_H_in_tAH_seq",100,0,1)));
  ANA_CHECK(book(TH1F("sin_t1_wrt_ttHxsin_lt1_wrt_H_in_t2H_seq","sin_t1_wrt_ttHxsin_lt1_wrt_H_in_t2H_seq",100,0,1)));

  countH = 0;
  countHbb = 0;
  countEv = 0;
  countWtaus = 0;
  countWdecay = 0;

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  ANA_MSG_INFO("in execute");
  
  hist("cutflow")->Fill(0);;

  // Retrieve event info
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo,"EventInfo"));

  vector<float> mcEventWeights = eventInfo->mcEventWeights();
  float mcEventWeight = mcEventWeights[0];
  //ANA_MSG_INFO(mcEventWeights[1]);

  /*  //const std::vector<std::string >& getWeightNames() const = 0;
  for(auto weight : m_weightTool->getWeightNames()){
  
    ANA_MSG_INFO(weight<<";"<<m_weightTool->getWeight(weight));
    
    }*/
 
 // Print event info
 ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
  /*
  // Retrieve boson container
  const xAOD::TruthParticleContainer *truthBosons = nullptr;
  ANA_CHECK(evtStore()->retrieve(truthBosons,"TruthBoson"));
  
  // Save Higgs boson with status 62
  const xAOD::TruthParticle *Higgs = nullptr;

  // Loop over truth bosons, check if they are all Higgs
  for(const xAOD::TruthParticle *boson : *truthBosons){

    if(boson->pdgId()==25) //ANA_MSG_INFO("Higgs status:"<<boson->status());

    if(boson->pdgId()==25 && boson->status()==62){
     
      hist("higgs_pt")->Fill(boson->pt()*0.001,mcEventWeight);    
      Higgs = boson;
      
      vector<ElementLink<xAOD::TruthParticleContainer>> children_vec = boson->auxdata< std::vector<ElementLink<xAOD::TruthParticleContainer> > >("childLinks");
      
      // Loop over Higgs daughters
      ANA_MSG_INFO("Looping over Higgs children");

      for(int i=0; i<children_vec.size(); i++){
	const ElementLink<xAOD::TruthParticleContainer> &child_i_link = children_vec[i]; 
	
	if(child_i_link.isValid()){
	  
	  const xAOD::TruthParticle* child_i = *child_i_link;
	  ANA_MSG_INFO(child_i->pdgId()<<";"<<child_i->status());
	  
	}
      }

    }
    
    if(TMath::Abs(boson->pdgId())==24 && boson->status()>=51){
     
      //ANA_MSG_INFO("W boson:"<<boson->pdgId()<<";"<<boson->status());
      vector<ElementLink<xAOD::TruthParticleContainer>> children_vec = boson->auxdata< std::vector<ElementLink<xAOD::TruthParticleContainer> > >("childLinks");
      
      // Loop over W daughters
      ANA_MSG_INFO("Looping over W children");

      for(int i=0; i<children_vec.size(); i++){
	const ElementLink<xAOD::TruthParticleContainer> &child_i_link = children_vec[i]; 
	
	if(child_i_link.isValid()){
	  
	  const xAOD::TruthParticle* child_i = *child_i_link;
	  ANA_MSG_INFO(child_i->pdgId()<<";"<<child_i->status()<<";"<<child_i->auxdata< unsigned int >( "classifierParticleType" )<<";"<<child_i->auxdata< unsigned int >( "classifierParticleOrigin" )<<";"<<child_i->auxdata< unsigned int >( "classifierParticleOutCome" ));
	  
	}
	}
    
    }
    }*/

  vector<const xAOD::TruthParticle*> Hbb_vec;
  const xAOD::TruthParticle* top_had = nullptr;
  const xAOD::TruthParticle* top_lep = nullptr;
  const xAOD::TruthParticle* W_had = nullptr;
  const xAOD::TruthParticle* W_lep = nullptr;
  const xAOD::TruthParticle* lep = nullptr;
  vector<const xAOD::TruthParticle*> quarksW_vec;
  const xAOD::TruthParticle* b_had = nullptr;
  const xAOD::TruthParticle* b_lep = nullptr;
  const xAOD::TruthParticle* b_aux = nullptr;
  const xAOD::TruthParticle* H = nullptr;
  const xAOD::TruthParticle* topA = nullptr;
  const xAOD::TruthParticle* topB = nullptr;
  const xAOD::TruthParticle* btopA = nullptr;
  const xAOD::TruthParticle* btopB = nullptr;
  const xAOD::TruthParticle* ltA = nullptr;

  bool found_lep_top = false;
  bool found_had_top = false;
  
  // TRUTH0
  // Retrieve Truth Particles
  const xAOD::TruthParticleContainer *truthParticles = nullptr;
  ANA_CHECK(evtStore()->retrieve(truthParticles,"TruthParticles"));

  int countHbb_aux = 0;

  // Loop over truth particles
  for(const xAOD::TruthParticle *part : *truthParticles){
    
    // Get Higgs with status 62
    if(part->pdgId()==25 && part->status()==62){

      H = part;

      countH += 1;
      // Get decay vertex link
      ElementLink<xAOD::TruthVertexContainer> vertexLink = part->auxdata<ElementLink<xAOD::TruthVertexContainer>>("decayVtxLink");                                            
      if(vertexLink.isValid()){
	
	const xAOD::TruthVertex* vertex = *vertexLink;                                                                                                                                                     
	// Get outgoing particles
	int nOutgoingParticles = vertex->nOutgoingParticles();
	// Loop over outgoing particles
	for(int i = 0; i<nOutgoingParticles; i++){

	  const xAOD::TruthParticle *p = vertex->outgoingParticle(i);
	  //ANA_MSG_INFO("pdg id:"<<p->pdgId()<<";status:"<<p->status());
	  
	  if(TMath::Abs(p->pdgId())==5){
	    countHbb_aux += 1; 
	    Hbb_vec.push_back(p);
	  }
	} // End of loop over outgoing particles from higgs decay vtx

	// We are only interested in events with H->bb
	if(Hbb_vec.size()!=2) return StatusCode::SUCCESS; 
	//ANA_MSG_INFO(Hbb_vec.size());
	if(countHbb_aux==2) countHbb += 1;

      }// End of if vertex link is valid
    }// End of if where we get Higgs with status 62

    // Get W
    /*if(TMath::Abs(part->pdgId())==24 && part->status()>=51){

            // Get decay vertex link
      ElementLink<xAOD::TruthVertexContainer> vertexLinkW = part->auxdata<ElementLink<xAOD::TruthVertexContainer>>("decayVtxLink");                                            
      if(vertexLinkW.isValid()){
	
	const xAOD::TruthVertex* vertexW = *vertexLinkW;                                                                                                                                                     
	// Get outgoing particles
	int nOutgoingParticles = vertexW->nOutgoingParticles();
	// Loop over outgoing particles
	ANA_MSG_INFO("Looping over W daughters");
	for(int i = 0; i<nOutgoingParticles; i++){

	  const xAOD::TruthParticle *p = vertexW->outgoingParticle(i);
	  ANA_MSG_INFO("pdg id:"<<p->pdgId()<<";status:"<<p->status());
	  
	} // End of loop over outgoing particles from higgs decay vtx
      }
      
      }*/

    // Get tops with status 62
    if(TMath::Abs(part->pdgId())==6 && part->status()==62){

      ElementLink<xAOD::TruthVertexContainer> vertexLinkTop = part->auxdata<ElementLink<xAOD::TruthVertexContainer>>("decayVtxLink");                                            
      if(vertexLinkTop.isValid()){

	const xAOD::TruthVertex* vertexTop = *vertexLinkTop;                                                                                                                                              
	// Get outgoing particles
	int nOutgoingParticles = vertexTop->nOutgoingParticles();
	// Loop over outgoing particles
	//ANA_MSG_INFO("Looping over top daughters");
	for(int i = 0; i<nOutgoingParticles; i++){
	  
	  const xAOD::TruthParticle *p = vertexTop->outgoingParticle(i);
	  //ANA_MSG_INFO("pdg id:"<<p->pdgId()<<";status:"<<p->status());

	  // Get b quarks from top decay
	  if(TMath::Abs(p->pdgId())==5){

	    b_aux = p;
	  }
  
	  if(TMath::Abs(p->pdgId())==24){
	    
	    int n = 1;
	    const xAOD::TruthVertex *v = nullptr;
	    const xAOD::TruthParticle *part1 = nullptr;
	    // Get decay vertex and outgoing particles
	    while(n == 1){
	      v = getDaughters(p);
	      n = v->nOutgoingParticles();
	      if(n == 1){
		part1 = v->outgoingParticle(0);
		p = part1;
	      }
	    }// End of while

	    if(v != NULL){
	      
	      const xAOD::TruthParticle *p1 = v->outgoingParticle(0);
	      if(TMath::Abs(p1->pdgId())<5){
		//ANA_MSG_INFO("FOUND HADRONIC TOP:"<<part->pdgId());
		found_had_top = true;
		W_had = p;
		top_had = part;
		b_had = b_aux;
		// Loop over W daughters and save quarks
		for(int j = 0; j<v->nOutgoingParticles(); j++){

		  quarksW_vec.push_back(v->outgoingParticle(j));

		}
	      }else if(TMath::Abs(p1->pdgId())>=11 && TMath::Abs(p1->pdgId())<=14){
		//ANA_MSG_INFO("FOUND LEPTONIC TOP:"<<part->pdgId());
		found_lep_top = true;
		W_lep = p;
		top_lep = part;
		b_lep = b_aux;
		// loop over W daughters and get lepton
		for(int j = 0; j<v->nOutgoingParticles(); j++){
		  
		  const xAOD::TruthParticle *pW = v->outgoingParticle(j);
		  //ANA_MSG_INFO(pW->pdgId());
		  if(TMath::Abs(pW->pdgId())==11 || TMath::Abs(pW->pdgId())==13){
		    lep = pW;
		  }else{
		    countWtaus += 1;
		    continue;
		  }
		}
       	      }else{
		countWdecay += 1;
		return StatusCode::SUCCESS;
	      }
	    }// End of if null vertex

	  }// End of if to find W from top

	  //if(top_had == NULL || top_lep == NULL) return StatusCode::SUCCESS;
	  
	}// End of loop over top outgoing particles

      }// End of if valid vtx link      

    }// End of if to get tops

  } // End of loop over truth particles

  // Find closest and further away tops from Higgs in dR    
  double dR_Htop_lep = TMath::Sqrt(TMath::Power(H->eta()-top_lep->eta(),2)+TMath::Power(H->phi()-top_lep->phi(),2));
  double dR_Htop_had = TMath::Sqrt(TMath::Power(H->eta()-top_had->eta(),2)+TMath::Power(H->phi()-top_had->phi(),2));

  // Sort b's from Higgs in pt
  vector<const xAOD::TruthParticle*> Hbb_vec_sorted = sort_vec_pt(Hbb_vec);
  // Sort quarks from W
  vector<const xAOD::TruthParticle*> quarksW_vec_sorted = sort_vec_pt(quarksW_vec);

  if(dR_Htop_lep<dR_Htop_had){

    topA = top_lep;
    topB = top_had;
  
    btopA = b_lep;
    btopB = b_had;

    ltA = lep;

  }else{

    topA = top_had;
    topB = top_lep;

    btopA = b_had;
    btopB = b_lep;

    ltA = quarksW_vec_sorted[1];

  }
  
  countEv += 1;
  //if(tops_vec.size() != 2) ANA_MSG_INFO("FOUND ONLY ONE TOP, ERROR SOMEWHERE PROBABLY");
  ANA_MSG_INFO("Higgs:"<<H->pdgId());
  ANA_MSG_INFO("b's from Higgs:"<<Hbb_vec[0]->pdgId()<<";"<<Hbb_vec[0]->status()<<";"<<Hbb_vec[1]->pdgId()<<";"<<Hbb_vec[1]->status());
  ANA_MSG_INFO("Hadronic top:"<<top_had->pdgId()<<";Leptonic top:"<<top_lep->pdgId());
  ANA_MSG_INFO("Hadronic W:"<<W_had->pdgId()<<";"<<W_had->status()<<";Leptonic W:"<<W_lep->pdgId()<<";"<<W_lep->status());
  ANA_MSG_INFO("Lepton:"<<lep->pdgId()<<";"<<lep->status());
  ANA_MSG_INFO("Vector of quarks size:"<<quarksW_vec.size()<<";"<<quarksW_vec[0]->pdgId()<<";"<<quarksW_vec[0]->status()<<";"<<quarksW_vec[1]->pdgId()<<";"<<quarksW_vec[1]->status());  
  ANA_MSG_INFO("b's from top:"<<b_had->pdgId()<<";"<<b_lep->pdgId());
  //ANA_MSG_INFO("Hadronic W:"<<W_vec.at(0)->pdgId()<<";Leptonic W:"<<W_vec.at(1));

  // Create TLorentzVectors
  TLorentzVector lep_lorentz = TLorentzVector();
  lep_lorentz.SetPtEtaPhiE(lep->pt()*0.001,lep->eta(),lep->phi(),lep->e()*0.001);
  TLorentzVector quarkW_highPt_lorentz = TLorentzVector();
  quarkW_highPt_lorentz.SetPtEtaPhiE(quarksW_vec_sorted[1]->pt()*0.001,quarksW_vec_sorted[1]->eta(),quarksW_vec_sorted[1]->phi(),quarksW_vec_sorted[1]->e()*0.001);
  TLorentzVector quarkW_lowPt_lorentz = TLorentzVector();
  quarkW_lowPt_lorentz.SetPtEtaPhiE(quarksW_vec_sorted[0]->pt()*0.001,quarksW_vec_sorted[0]->eta(),quarksW_vec_sorted[0]->phi(),quarksW_vec_sorted[0]->e()*0.001);

  TLorentzVector tA_lorentz = TLorentzVector();
  tA_lorentz.SetPtEtaPhiE(topA->pt()*0.001,topA->eta(),topA->phi(),topA->e()*0.001);
  TLorentzVector tB_lorentz = TLorentzVector();
  tB_lorentz.SetPtEtaPhiE(topB->pt()*0.001,topB->eta(),topB->phi(),topB->e()*0.001);

  TLorentzVector thad_lorentz = TLorentzVector();
  thad_lorentz.SetPtEtaPhiE(top_had->pt()*0.001,top_had->eta(),top_had->phi(),top_had->e()*0.001);
  TLorentzVector tlep_lorentz = TLorentzVector();
  tlep_lorentz.SetPtEtaPhiE(top_lep->pt()*0.001,top_lep->eta(),top_lep->phi(),top_lep->e()*0.001);

  TLorentzVector Whad_lorentz = TLorentzVector();
  Whad_lorentz.SetPtEtaPhiE(W_had->pt()*0.001,W_had->eta(),W_had->phi(),W_had->e()*0.001);
  TLorentzVector Wlep_lorentz = TLorentzVector();
  Wlep_lorentz.SetPtEtaPhiE(W_lep->pt()*0.001,W_lep->eta(),W_lep->phi(),W_lep->e()*0.001);

  TLorentzVector btA_lorentz = TLorentzVector();
  btA_lorentz.SetPtEtaPhiE(btopA->pt()*0.001,btopA->eta(),btopA->phi(),btopA->e()*0.001);  
  TLorentzVector btB_lorentz = TLorentzVector();
  btB_lorentz.SetPtEtaPhiE(btopB->pt()*0.001,btopB->eta(),btopB->phi(),btopB->e()*0.001);  
  
  TLorentzVector H_lorentz = TLorentzVector();
  H_lorentz.SetPtEtaPhiE(H->pt()*0.001,H->eta(),H->phi(),H->e()*0.001);

  TLorentzVector bthad_lorentz = TLorentzVector();
  bthad_lorentz.SetPtEtaPhiE(b_had->pt()*0.001,b_had->eta(),b_had->phi(),b_had->e()*0.001);
  TLorentzVector btlep_lorentz = TLorentzVector();
  btlep_lorentz.SetPtEtaPhiE(b_lep->pt()*0.001,b_lep->eta(),b_lep->phi(),b_lep->e()*0.001);  

  TLorentzVector bH_highPt_lorentz = TLorentzVector();
  bH_highPt_lorentz.SetPtEtaPhiE(Hbb_vec_sorted[1]->pt()*0.001,Hbb_vec_sorted[1]->eta(),Hbb_vec_sorted[1]->phi(),Hbb_vec_sorted[1]->e()*0.001);
  TLorentzVector bH_lowPt_lorentz = TLorentzVector();
  bH_lowPt_lorentz.SetPtEtaPhiE(Hbb_vec_sorted[0]->pt()*0.001,Hbb_vec_sorted[0]->eta(),Hbb_vec_sorted[0]->phi(),Hbb_vec_sorted[0]->e()*0.001);

  TLorentzVector ltA_lorentz = TLorentzVector();
  ltA_lorentz.SetPtEtaPhiE(ltA->pt()*0.001,ltA->eta(),ltA->phi(),ltA->e()*0.001);

  vector<TLorentzVector> bquarks_vec;
  bquarks_vec.push_back(bthad_lorentz);
  bquarks_vec.push_back(btlep_lorentz);
  bquarks_vec.push_back(bH_highPt_lorentz);
  bquarks_vec.push_back(bH_lowPt_lorentz);

  vector<TLorentzVector> quarks_vec;
  quarks_vec.push_back(bthad_lorentz); 
  quarks_vec.push_back(btlep_lorentz);
  quarks_vec.push_back(bH_highPt_lorentz);
  quarks_vec.push_back(bH_lowPt_lorentz);
  quarks_vec.push_back(quarkW_highPt_lorentz);
  quarks_vec.push_back(quarkW_lowPt_lorentz);

  // dEtabb_MaxdEta_Sort4
  // From all b quarks in event (from higgs and tops) choose the pair that maximizes dEta
  double dEtabb = 0;
  double dEtabbMax = 0;
  for(int i = 0; i<bquarks_vec.size(); i++){
    
    for(int j = i; j<bquarks_vec.size(); j++){

      dEtabb = TMath::Abs(bquarks_vec[i].Eta()-bquarks_vec[j].Eta());
      
      if(dEtabb>dEtabbMax) dEtabbMax = dEtabb;

    }

  }

  hist("dEta_bb")->Fill(dEtabbMax,mcEventWeight);

  // dEtajl_MaxdEta
  // Loop over all quarks
  double dEtajl = 0;
  double dEtajlMax = 0;
  for(int i = 0; i<quarks_vec.size(); i++){

    dEtajl = TMath::Abs(quarks_vec[i].Eta()-lep_lorentz.Eta());

    if(dEtajl>dEtajlMax) dEtajlMax = dEtajl; 

  }

  hist("dEta_jl")->Fill(dEtajlMax,mcEventWeight);

  // Sort vectors of bquarks and quarks
  sort(bquarks_vec.begin(),bquarks_vec.end(),sort_vec_pt_lorentz);
  sort(quarks_vec.begin(),quarks_vec.end(),sort_vec_pt_lorentz);

  //ANA_MSG_INFO(bquarks_vec[0].Pt()<<";"<<bquarks_vec[1].Pt()<<";"<<bquarks_vec[2].Pt()<<";"<<bquarks_vec[3].Pt());

  hist("m_ljjjjj_MaxPt")->Fill((lep_lorentz+quarks_vec[0]+quarks_vec[1]+quarks_vec[2]+quarks_vec[3]+quarks_vec[4]).M(),mcEventWeight);
  
  double Ptbb = 0;
  double dRbb = 0;
  double Mbb = 0;

  double PtbbMax = 0;
  double dRbbMin = 9e9;
  double MbbMax = 0;  

  double Mbb_MaxPt = 0;
  double Mbb_MindR = 0;

  for(int i = 0; i<bquarks_vec.size(); i++){
    
    for(int j = i+1; j<bquarks_vec.size(); j++){
      
      Ptbb = (bquarks_vec[i]+bquarks_vec[j]).Pt();

      if(Ptbb>PtbbMax){

	PtbbMax = Ptbb;
	Mbb_MaxPt = (bquarks_vec[i]+bquarks_vec[j]).M();

      }

      dRbb = TMath::Sqrt(TMath::Power(bquarks_vec[i].Eta()-bquarks_vec[j].Eta(),2)+TMath::Power(bquarks_vec[i].Phi()-bquarks_vec[j].Phi(),2));

      if(dRbb<dRbbMin){

	dRbbMin = dRbb;
	Mbb_MindR = (bquarks_vec[i]+bquarks_vec[j]).M();
	
      }

      Mbb = (bquarks_vec[i]+bquarks_vec[j]).M();
      if(Mbb>MbbMax) MbbMax = Mbb;

    }

  }

  hist("Mbb_MaxPt_Sort4")->Fill(Mbb_MaxPt,mcEventWeight);
  hist("Mbb_MindR_Sort4")->Fill(Mbb_MindR,mcEventWeight);
  hist("Mbb_MaxM_Sort4")->Fill(MbbMax,mcEventWeight);

  hist("higgs_pt")->Fill(H_lorentz.Pt(),mcEventWeight);
  hist("thad_pt")->Fill(thad_lorentz.Pt(),mcEventWeight);
  hist("tlep_pt")->Fill(tlep_lorentz.Pt(),mcEventWeight);
  hist("lep_pt")->Fill(lep_lorentz.Pt(),mcEventWeight);
  hist("dEta_tt")->Fill(TMath::Abs(thad_lorentz.Eta()-tlep_lorentz.Eta()),mcEventWeight);
  hist("dEta_bb")->Fill(TMath::Abs(bH_lowPt_lorentz.Eta()-bH_highPt_lorentz.Eta()),mcEventWeight);
  hist("dPhi_tt")->Fill(thad_lorentz.DeltaPhi(tlep_lorentz),mcEventWeight);
  hist("b4_tt")->Fill(b4(thad_lorentz,tlep_lorentz),mcEventWeight);

  // Calculate variables
  // 6j regions
  // b4
  float b4_lep_quarkWhighPt = b4(lep_lorentz,quarkW_highPt_lorentz);
  float sin_tB_wrt_ttH = cos_theta_1_wrt_123(tB_lorentz,tA_lorentz,H_lorentz);
  float sin_btA_wrt_H_in_tAH_seq = cos_theta_4_wrt_3seq(tB_lorentz,tA_lorentz,H_lorentz,btA_lorentz);
  float dR_bH_highPt_bhad = TMath::Sqrt(TMath::Power(bH_highPt_lorentz.Eta()-bthad_lorentz.Eta(),2)+TMath::Power(bH_highPt_lorentz.Phi()-bthad_lorentz.Phi(),2));
  float dR_lep_bH_highPt = TMath::Sqrt(TMath::Power(lep_lorentz.Eta()-bH_highPt_lorentz.Eta(),2)+TMath::Power(lep_lorentz.Phi()-bH_highPt_lorentz.Phi(),2));
  float M_Wlep_bthad = (Wlep_lorentz+bthad_lorentz).M();
  float cos_b1H_wrt_tA_in_HtA_seq = cos_theta_4_wrt_3seq(tB_lorentz,H_lorentz,tA_lorentz,bH_highPt_lorentz);
  float cos_b2H_wrt_tA_in_HtA_seq = cos_theta_4_wrt_3seq(tB_lorentz,H_lorentz,tA_lorentz,bH_lowPt_lorentz); 
  float sin_t1_wrt_ttH = cos_theta_1_wrt_123(tlep_lorentz,thad_lorentz,H_lorentz);
  float sin_Wt2_wrt_H_in_t1H_seq = cos_theta_4_wrt_3seq(thad_lorentz,tlep_lorentz,H_lorentz,Whad_lorentz);

  // Fill histos
  hist("b4_lep_quarkWhighPt")->Fill(b4_lep_quarkWhighPt,mcEventWeight);
  hist("sin_tB_wrt_ttHxsin_btA_wrt_H_in_tAH_seq")->Fill(sin_tB_wrt_ttH*sin_btA_wrt_H_in_tAH_seq,mcEventWeight);
  hist("dR_bH_highPt_bhad")->Fill(dR_bH_highPt_bhad,mcEventWeight);
  hist("dR_lep_bH_highPt")->Fill(dR_lep_bH_highPt,mcEventWeight);
  hist("M_Wlep_bthad")->Fill(M_Wlep_bthad,mcEventWeight);
  hist("cos_b1H_wrt_tA_in_HtA_seqxcos_b2H_wrt_tA_in_HtA_seq")->Fill(cos_b1H_wrt_tA_in_HtA_seq*cos_b2H_wrt_tA_in_HtA_seq,mcEventWeight);
  hist("sin_t1_wrt_ttHxsin_Wt2_wrt_H_in_t1H_seq")->Fill(sin_t1_wrt_ttH*sin_Wt2_wrt_H_in_t1H_seq,mcEventWeight);

  // 5j regions
  float dR_H_tlep = TMath::Sqrt(TMath::Power(H_lorentz.Eta()-tlep_lorentz.Eta(),2)+TMath::Power(H_lorentz.Phi()-tlep_lorentz.Phi(),2)); 
  float M_H_btlep = (H_lorentz+btlep_lorentz).M();
  float sin_ltA_wrt_H_in_tAH_seq = cos_theta_4_wrt_3seq(tB_lorentz,tA_lorentz,H_lorentz,ltA_lorentz);
  //float sin_t1_wrt_ttH = cos_theta_1_wrt_123(tlep_lorentz,thad_lorentz,H_lorentz);
  float sin_lt1_wrt_H_in_t2H_seq = cos_theta_4_wrt_3seq(tlep_lorentz,thad_lorentz,H_lorentz,lep_lorentz);

  // Fill histos
  hist("dR_H_tlep")->Fill(dR_H_tlep,mcEventWeight);
  hist("M_H_btlep")->Fill(M_H_btlep,mcEventWeight);
  hist("sin_ltA_wrt_H_in_tAH_seq")->Fill(sin_ltA_wrt_H_in_tAH_seq,mcEventWeight);
  hist("sin_t1_wrt_ttHxsin_lt1_wrt_H_in_t2H_seq")->Fill(sin_t1_wrt_ttH*sin_lt1_wrt_H_in_t2H_seq,mcEventWeight);

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  ANA_MSG_INFO(countH<<";"<<countHbb);
  ANA_MSG_INFO("No. of lep W decaying to taus:"<<countWtaus);
  ANA_MSG_INFO("No. of W not decaying to quarks or e/mu:"<<countWdecay);
  ANA_MSG_INFO("No. events after cuts:"<<countEv);

  return StatusCode::SUCCESS;
}

vector<const xAOD::TruthParticle*> MyxAODAnalysis::sort_vec_pt(vector<const xAOD::TruthParticle*> vec){


  if(vec[0]->pt()>vec[1]->pt()) swap(vec[0],vec[1]);
  
  return vec; 

}

bool MyxAODAnalysis::sort_vec_pt_lorentz(TLorentzVector vec1, TLorentzVector vec2){


  return (vec1.Pt()<vec2.Pt());

}

const xAOD::TruthVertex* MyxAODAnalysis :: getDaughters(const xAOD::TruthParticle *part){

  ANA_MSG_INFO("Accessing daughters");
  int nOutgoingParticles = 1;
  const xAOD::TruthVertex* vertex = nullptr;
  ElementLink<xAOD::TruthVertexContainer> vertexLink = part->auxdata<ElementLink<xAOD::TruthVertexContainer>>("decayVtxLink");                                            
  if(vertexLink.isValid()){
    
    vertex = *vertexLink;                                                                                                                                                     
    // Get outgoing particles
    nOutgoingParticles = vertex->nOutgoingParticles();
    ANA_MSG_INFO("No. of daughters:"<<nOutgoingParticles);
    // Loop over outgoing particles
    for(int i = 0; i<nOutgoingParticles; i++){
      
      const xAOD::TruthParticle *p = vertex->outgoingParticle(i);
      ANA_MSG_INFO("pdg id:"<<p->pdgId()<<";status:"<<p->status());
   
    } // End of loop over outgoing particles from higgs decay vtx
   
  }// End of if vertex link is valid

  return vertex;
  
}

float MyxAODAnalysis :: b4(TLorentzVector v1, TLorentzVector v2){
  // ----------------------------------------------------------------------------------------------------------------
  // Define Function to Calculate b4 variable
  // ----------------------------------------------------------------------------------------------------------------

  // Check if input is sensible
  if(v1.Px()==0 && v1.Py()==0 && v1.Pz()==0) return -5;
  if(v2.Px()==0 && v2.Py()==0 && v2.Pz()==0) return -5;
  if(v1.Px()==-99 && v1.Py()==-99 && v1.Pz()==-99) return -5;
  if(v2.Px()==-99 && v2.Py()==-99 && v2.Pz()==-99) return -5;

  float b4 = (v1.Pz()*v2.Pz())/(v1.P()*v2.P());
  // If we have a nan return default value -5
  if (isnan(b4)){
    return -5.;
  } else {
    return b4;
  }
}

float MyxAODAnalysis :: cos_theta_1_wrt_123(TLorentzVector v1, TLorentzVector v2, TLorentzVector v3){
  // ----------------------------------------------------------------------------------------------------------------
  // Define Function to Calculate Angle of v1 in ttH centre-of-mass
  // Inputs: v1, v2, v3 TLorentzVector(s) for objects in LAB system
  // Execution:
  //   I] Evaluate Ang 1
  //      1. Create S' = (v1+v2+v3) center-of-mass system;
  //      2. Boost v1 object to S' and creat v1'
  //      3. Calculate Ang1 = the angle of v1'(in S') with respect to direction of (v1+v2+v3) system in LAB
  // ----------------------------------------------------------------------------------------------------------------

  // Check if input is sensible
  if(v1.Px()==0 && v1.Py()==0 && v1.Pz()==0) return -5;
  if(v2.Px()==0 && v2.Py()==0 && v2.Pz()==0) return -5;
  if(v3.Px()==0 && v3.Py()==0 && v3.Pz()==0) return -5;
  if(v1.Px()==-99 && v1.Py()==-99 && v1.Pz()==-99) return -5;
  if(v2.Px()==-99 && v2.Py()==-99 && v2.Pz()==-99) return -5;
  if(v3.Px()==-99 && v3.Py()==-99 && v3.Pz()==-99) return -5;

  //   I] Evaluate Ang 1
  TLorentzVector     cm123 = v1 + v2 + v3;                // centre-of-mass system
  TVector3    boost_vec123 = -(cm123).BoostVector();      // define boost vector
  TLorentzVector  v1_cm123;                               // 4-vec boosted to (1+2+3)
  //__v1______
  v1_cm123 = v1;
  v1_cm123.Boost(boost_vec123);
  // Calculate cos_theta
  float cos_theta = v1_cm123.Vect().Unit().Dot(cm123.Vect().Unit());
  // If we have a nan return default value -5
  if (isnan(cos_theta)){
    return -5.;
  } else {
    return cos_theta;
  }
}

float MyxAODAnalysis :: cos_theta_3_wrt_23(TLorentzVector v1, TLorentzVector v2, TLorentzVector v3){
  // ----------------------------------------------------------------------------------------------------------------
  // Define Function to Calculate Angle of 2 in (2,3) centre-of-mass
  // Inputs:   v1, v2, v3 TLorentzVector(s) for objects in LAB system
  // Execution:
  //   I] Evaluate Ang 1
  //      1. Create S' = (v1+v2+v3) center-of-mass system
  //      2. Boost v2,v3 objects to S' and create v2',v3'
  //      3. Create S'' = (v2'+v3') centre-of-mass system
  //      4. Boost v3' to S'' and create v3''
  //      5. Calculate Ang1 = the angle of v3''(in S'') with respect to direction of (v2'+v3') system in S'
  // ----------------------------------------------------------------------------------------------------------------

  // Check if input is sensible
  if(v1.Px()==0 && v1.Py()==0 && v1.Pz()==0) return -5;
  if(v2.Px()==0 && v2.Py()==0 && v2.Pz()==0) return -5;
  if(v3.Px()==0 && v3.Py()==0 && v3.Pz()==0) return -5;
  if(v1.Px()==-99 && v1.Py()==-99 && v1.Pz()==-99) return -5;
  if(v2.Px()==-99 && v2.Py()==-99 && v2.Pz()==-99) return -5;
  if(v3.Px()==-99 && v3.Py()==-99 && v3.Pz()==-99) return -5;


  //   I] Evaluate Ang 1
  TLorentzVector     cm123 = v1 + v2 + v3;                // centre-of-mass system
  TVector3    boost_vec123 = -(cm123).BoostVector();      // define boost vector
  TLorentzVector  v2_cm123, v3_cm123;           // 4-vec boosted to (1+2+3)
  //__v2______
  v2_cm123 = v2;
  v2_cm123.Boost(boost_vec123);
  //__v3______
  v3_cm123 = v3;
  v3_cm123.Boost(boost_vec123);
  //
  // -----------------------------------------------------------
  TLorentzVector     cm23 = v2_cm123 + v3_cm123;          // centre-of-mass system
  TVector3    boost_vec23 = -(cm23).BoostVector();        // define boost vector
  TLorentzVector  v3_cm23;                                // 4-vec boosted to (2+3)
  //__v3______
  v3_cm23 = v3_cm123;
  v3_cm23.Boost(boost_vec23);
  // Calculate cos_theta
  float cos_theta = v3_cm23.Vect().Unit().Dot(cm23.Vect().Unit());
  // If we have a nan return default value -5
  if (isnan(cos_theta)){
    return -5.;
  } else {
    return cos_theta;
  }
}

float MyxAODAnalysis :: cos_theta_4_wrt_3(TLorentzVector v1, TLorentzVector v2, TLorentzVector v3, TLorentzVector v4){
  // ----------------------------------------------------------------------------------------------------------------
  // Define Function to Calculate Angle of 4 in 3 centre-of-mass
  // Inputs:   v1, v2, v3, v4 TLorentzVector(s) for objects in LAB system
  // Execution:
  //   I] Evaluate four-vectors in centre of mass (v1+v2+v3)
  //      1. Create S' = (v1+v2+v3) center-of-mass system
  //      2. Boost v2,v3 objects to S' and create v2',v3'
  //  II] Evaluate Ang 2
  //      1. Create S'' = (v2'+v3') centre-of-mass system
  //      2. Boost v3' to S'' and create v3''
  //      3. Boost v4 (in LAB) directly to v3 (measured in LAB) centre-of-mass system and create v4'''
  //      4. Calculate Ang 2 = the angle between v4''' and v3''
  // ----------------------------------------------------------------------------------------------------------------

  // Check if input is sensible
  if(v1.Px()==0 && v1.Py()==0 && v1.Pz()==0) return -5;
  if(v2.Px()==0 && v2.Py()==0 && v2.Pz()==0) return -5;
  if(v3.Px()==0 && v3.Py()==0 && v3.Pz()==0) return -5;
  if(v4.Px()==0 && v4.Py()==0 && v4.Pz()==0) return -5;
  if(v1.Px()==-99 && v1.Py()==-99 && v1.Pz()==-99) return -5;
  if(v2.Px()==-99 && v2.Py()==-99 && v2.Pz()==-99) return -5;
  if(v3.Px()==-99 && v3.Py()==-99 && v3.Pz()==-99) return -5;
  if(v4.Px()==-99 && v4.Py()==-99 && v4.Pz()==-99) return -5;


  //   I] Evaluate four-vectors in centre of mass (v1+v2+v3)
  TLorentzVector     cm123 = v1 + v2 + v3;                // centre-of-mass system
  TVector3    boost_vec123 = -(cm123).BoostVector();      // define boost vector
  TLorentzVector  v2_cm123, v3_cm123;                     // 4-vec boosted to (1+2+3)
  //__v2______
  v2_cm123 = v2;
  v2_cm123.Boost(boost_vec123);
  //__v3______
  v3_cm123 = v3;
  v3_cm123.Boost(boost_vec123);

  //  II] Evaluate Ang 2
  TLorentzVector     cm23 = v2_cm123 + v3_cm123;          // centre-of-mass system
  TVector3    boost_vec23 = -(cm23).BoostVector();        // define boost vector
  TLorentzVector  v3_cm23;                                // 4-vec boosted to (2+3)
  //__v3______
  v3_cm23 = v3_cm123;
  v3_cm23.Boost(boost_vec23);
  // boost 4 (at LAB) directly to centre-of-mass of 3 (in LAB)
  TLorentzVector  v4_cm3;                                 // 4-vec (LAB) boosted to 3 (LAB)
  TVector3    boost_vec3 = -(v3).BoostVector();           // define boost vector of 3 (LAB)
  //__v4______
  v4_cm3 = v4;
  v4_cm3.Boost(boost_vec3);
  // Calculate cos_theta
  float cos_theta = v4_cm3.Vect().Unit().Dot(v3_cm23.Vect().Unit());
  // If we have a nan return default value -5
  if (isnan(cos_theta)){
    return -5.;
  } else {
    return cos_theta;
  }
}

float MyxAODAnalysis :: cos_theta_4_wrt_3seq(TLorentzVector v1, TLorentzVector v2, TLorentzVector v3, TLorentzVector v4){
  // ----------------------------------------------------------------------------------------------------------------
  // Define Function to Calculate Angle of 4 in (2,3) centre-of-mass
  // Inputs:   v1, v2, v3, v4 TLorentzVector(s) for objects in LAB system
  // Execution:
  //   I] Evaluate four-vectors in centre of mass (v1+v2+v3)
  //      1. Create S' = (v1+v2+v3) center-of-mass system
  //      2. Boost v2,v3,v4 objects to S' and create v2',v3',v4'
  //  II] Evaluate Ang 2
  //      1. Create S'' = (v2'+v3') centre-of-mass system
  //      2. Boost v3',v4' to S'' and create v3'',v4''
  //      3. Boost v4'' to v3'' and create v4'''
  //      4. Calculate Ang 2 = the angle between v4''' and v3''
  // ----------------------------------------------------------------------------------------------------------------

  // Check if input is sensible
  if(v1.Px()==0 && v1.Py()==0 && v1.Pz()==0) return -5;
  if(v2.Px()==0 && v2.Py()==0 && v2.Pz()==0) return -5;
  if(v3.Px()==0 && v3.Py()==0 && v3.Pz()==0) return -5;
  if(v4.Px()==0 && v4.Py()==0 && v4.Pz()==0) return -5;
  if(v1.Px()==-99 && v1.Py()==-99 && v1.Pz()==-99) return -5;
  if(v2.Px()==-99 && v2.Py()==-99 && v2.Pz()==-99) return -5;
  if(v3.Px()==-99 && v3.Py()==-99 && v3.Pz()==-99) return -5;
  if(v4.Px()==-99 && v4.Py()==-99 && v4.Pz()==-99) return -5;


  //   I] Evaluate four-vectors in centre of mass (v1+v2+v3)
  TLorentzVector     cm123 = v1 + v2 + v3;                // centre-of-mass system
  TVector3    boost_vec123 = -(cm123).BoostVector();      // define boost vector
  TLorentzVector v2_cm123, v3_cm123, v4_cm123;            // 4-vec boosted to (1+2+3)
  //__v2______
  v2_cm123 = v2;
  v2_cm123.Boost(boost_vec123);
  //__v3______
  v3_cm123 = v3;
  v3_cm123.Boost(boost_vec123);
  //__v4______
  v4_cm123 = v4;
  v4_cm123.Boost(boost_vec123);

  //  II] Evaluate Ang 2
  TLorentzVector     cm23 = v2_cm123 + v3_cm123;          // centre-of-mass system
  TVector3    boost_vec23 = -(cm23).BoostVector();        // define boost vector
  TLorentzVector  v3_cm23, v4_cm23;                       // 4-vec boosted to (2+3)
  //__v3______
  v3_cm23 = v3_cm123;
  v3_cm23.Boost(boost_vec23);
  //__v4______
  v4_cm23 = v4_cm123;
  v4_cm23.Boost(boost_vec23);
  // boost 4'' to centre-of-mass of 3''
  TLorentzVector  v4_cm3;                                    // 4-vec (LAB) boosted to 3 (LAB)
  TVector3    boost_vec3 = -(v3_cm23).BoostVector();         // define boost vector of 3 (LAB)
  //__v4______
  v4_cm3 = v4_cm23;
  v4_cm3.Boost(boost_vec3);
  // Calculate cos_theta
  float cos_theta = v4_cm3.Vect().Unit().Dot(v3_cm23.Vect().Unit());
  // If we have a nan return default value -5
  if (isnan(cos_theta)){
    return -5.;
  } else {
    return cos_theta;
  }
}
