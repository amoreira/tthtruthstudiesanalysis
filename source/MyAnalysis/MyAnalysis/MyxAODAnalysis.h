#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/AnaToolHandle.h>

#include <PMGTools/PMGTruthWeightTool.h>

class MyxAODAnalysis : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

 private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;
  asg::AnaToolHandle<PMGTools::IPMGTruthWeightTool> m_weightTool;

  int countH;
  int countHbb;
  int countEv;
  int countWdecay;
  int countWtaus;

  // CP variables
  float b4(TLorentzVector, TLorentzVector);
  float cos_theta_1_wrt_123(TLorentzVector, TLorentzVector, TLorentzVector);
  float cos_theta_3_wrt_23(TLorentzVector, TLorentzVector, TLorentzVector);
  float cos_theta_4_wrt_3(TLorentzVector, TLorentzVector, TLorentzVector, TLorentzVector);
  float cos_theta_4_wrt_3seq(TLorentzVector, TLorentzVector, TLorentzVector, TLorentzVector);

  const xAOD::TruthVertex* getDaughters(const xAOD::TruthParticle*);

  std::vector<const xAOD::TruthParticle*> sort_vec_pt(std::vector<const xAOD::TruthParticle*>);

  static bool sort_vec_pt_lorentz(TLorentzVector,TLorentzVector);

};

#endif
