# Studies on the validity of the linear interpolation between CP-even and CP-odd ttH(H->bb, semileptonic and dileptonic)

## Get code, compile and run
These instructions are general. They should work out of the box on lxplus or any local system (like the Fermi machines at LIP)

**First time:**
- login into the fermi machines
- add the following lines to your .bashrc file:
    - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    - alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
- setupATLAS
- git clone https://gitlab.cern.ch/amoreira/tthtruthstudiesanalysis.git
- cd tthtruthstudiesanalysis
- git checkout Dileptonic
- mkdir build (where you compile the code)
- mkdir run (where you run the code)
- cd build
- asetup 21.2.68,AnalysisBase
- cmake ../source
- make
- source */setup.sh
- cd ../run
- ATestRun_eljob.py --submission-dir=test

**Following times:**
- login into the fermi machines
- setupATLAS
- cd tthtruthstudiesanalysis/build
- asetup --restore
- make
- cd ../run
- ATestRun_eljob.py --submission-dir=test

## (Very) Basic structure of the code

- **Main file**: tthtruthstudiesanalysis/source/MyAnalysis/Root/MyxAODAnalysis.cxx
    - Start by taking a look at this file
    - It has three main functions: initialize(), finalize() and execute()
    - As the name indicates, execute() is the function that actually takes the input sample and does stuff
- **Configuration file**: tthtruthstudiesanalysis/source/MyAnalysis/share/ATestRun_eljob.py
    - This file allows you to specify, for example, over which samples to run over

